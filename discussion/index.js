
let gwaPerSubj = [98.5, 94.3, 89.2, 90.0];

console.log(gwaPerSubj);

/*
Objects
	collection of related data & functionalities(identifiers); ususally represents real world objects

*/
let grade = {
	//curly braces - initializer for creating objects
	//key-value pair:
	/*
	key-field/identifier to describe data/value;
	value-info that serves as value of the key
	*/
	math:98.5,
	english:94.3,
	science:90.0,
	MAPEH:89.2
};
console.log(grade);
//console.log(gwaPerSubj[3]); accessing array

/*
accessing elements inside the object
	syntax for creating an object
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}

	Dot Notation - used to access specific property & value of object; preferred in terms of object access

	objectName.key
*/

console.log(grade.english); //dot notation for accessing object
console.log(grade.english, grade.math); //accessing 2 keys in the object


let cellphone = {
	brand:"Oppo",
	color:"Blue",
	mfd:2021,
};
console.log(cellphone);
console.log(typeof cellphone); //determines data type of element

//NESTED ELEMENTS
let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 09123456789,
	location:{
		city:"Tokyo",
		country: "Japan",
	},
	email:["john@mail.com", "john.smith@mail.com"],

	fullName:(function name(fullName) {
		//this - object identifier where it is inserted
		/*
		in this case, code below could be typed as console.log(student.firstName + " " + student.lastName)
		*/
		console.log(this.firstName + " " + this.lastName)
	})
};
student.fullName();

console.log(student.location.city);
console.log(student.email);
console.log(student.email[0]);
console.log(student.email[1]);

/*
ARRAY OF OBJECTS
[array brackets containing{curly braces for objects}]
*/
let contactList = [
{
	firstName:"John",
	lastName: "Smith",
	location: "Japan"
},
{
	firstName: "Jane",
	lastName: "Smith",
	location: "Japan"
},
{
	firstName: "Jasmine",
	lastName: "Smith",
	location: "Japan"
}
];
console.log(contactList[1]); //access element in array
console.log(contactList[2].firstName); //access key in object inside array

/*
Constructor Function - create reusable function to create several objects
	- useful for creating copies/instances of an object
SYNTAX:
	function objectName (keyA, keyB){
	this.keyA = keyA,
	this.keyB = keyB
	}

object literals - let objectName = {}

instance - concrete occurence of any object w/c emphasizes on unique identity of it


*/

//Reusable Function - Object Method
//function name will be the object name for every instance/copy
function Laptop(name, manufacturedDate){
	this.name = name,
	this.manufacturedDate = manufacturedDate
};
//console.log(this) - it also performs different commands

//new keyword signifies that there will be a new object under the Laptop function (function to create new object)
let laptop1 = new Laptop("Lenovo", 2008);
console.log(laptop1);

let laptop2 = new Laptop("Toshiba", 1997);
console.log(laptop2);

let laptopAll = [laptop1, laptop2];
console.log(laptopAll);

/*
initializing, adding, deleting, reassigning of object properties
*/

let car = {};
console.log(car);
//adding object properties - same as accessing an object

car.name = "Honda Civic";
console.log(car);

car["manufacturedDate"] = 2020; //another way to insert object [through brackets] aside from dot notation (car.name)
console.log(car);

//reassigning object (same/existing property but assigning diff value)
//car["name"] = "Volvo";
car.name = "Volvo";
console.log(car);

//deleting object properties-can do it 2 ways
//delete car.manufacturedDate;
delete car["manufacturedDate"];
console.log(car);

let person = {
	name: "John",
	talk: function(){ //talk has value of function
		console.log('Hi! My name is ' + this.name)
	}
}
/* LOG IN THE CONSOLE:
person.talk()
index.js:158 Hi! My name isJohn
*/

//add property
person.walk = function(){
	console.log(this.name + "walked 25 steps forward.");
};

let friend = {
	firstName: "Joe",
	lastName: "Doe",
	address:{
		city: "Austin",
		state: "Texas"
	},
	emails:["joe@mail.com", "joedoe@mail.com"],
	introduce: function(){
		console.log("Hello! My name is "+ this.firstName + " " + this.lastName)
	}
};

let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function (){
		console.log("This pokemon tackeld targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
}

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
	// Skills of Pokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}
let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);