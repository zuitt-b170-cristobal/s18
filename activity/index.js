let trainer = {
	name: "Wallace",
	age: 30,
	pokemon: ["Milotic", "Sealeo", "Seaking"],
	friends: {
			ashKetchum: ["Pikachu", "Charizard"],
			Drake: ["Shelgon", "Altaria"]
			},
	talk: function (){
		console.log("Pikachu! I choose you!")
	}
}

/*
let pokemon = {
	name: "Milotic",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function (){
		console.log("This pokemon tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
}
*/
function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
	// Skills of Pokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		difference = target.health - this.attack;
		var status = "";
		status += target.name + " \'s health is now reduced to " + difference;  
		return status;
	}
	this.faint = function(target){
		if (difference <= 0) {
		//var result = "";
		//result += target.name + " fainted";
		//return result;
		console.log(target.name + " fainted");
		}
	 }
}	
let Milotic = new Pokemon ("Milotic", 6);
let Sealeo = new Pokemon ("Sealeo", 4);
let Seaking = new Pokemon ("Seaking", 3);
let Pikachu = new Pokemon ("Pikachu", 6);
let Charizard = new Pokemon("Charizard", 4);
let Shelgon = new Pokemon("Shelgon", 5);
let Altaria = new Pokemon("Altaria", 1);

